﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmployeeManagement {

    public partial class Form1 : Form {

        string path = @"Employee_data.csv";

        public Form1() {
            InitializeComponent();
        }

        private void ReadCsvFile(StreamReader streamReader) {
            using(CsvReader csvReader = new CsvReader(streamReader)) {
                List<Employee> data = csvReader.GetRecords<Employee>().ToList();
                if(data.Count > 0) {
                    employeeBindingSource.DataSource = data;
                }
            }
        }

        private void WriteCsvFile(StreamWriter streamWriter) {
            using(CsvWriter cw = new CsvWriter(streamWriter)) {
                cw.WriteHeader<Employee>();
                List<Employee> lst = employeeBindingSource.DataSource as List<Employee>;
                if(lst.Count > 0) {
                    foreach(Employee emp in lst) {
                        //Console.WriteLine(emp.name + " " + emp.dob);
                        cw.WriteRecord<Employee>(emp);
                    }
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e) {
            employeeBindingSource.DataSource = new List<Employee>();

            if(!File.Exists(path)) {
                File.Create(path).Close();
                using(StreamWriter sw = new StreamWriter(new FileStream(path, FileMode.Open))) {
                    using(CsvWriter cw = new CsvWriter(sw)) {
                        cw.WriteHeader<Employee>();
                    }
                }
            }

            try {
                using(StreamReader sr = new StreamReader(new FileStream(path, FileMode.Open))) {
                    ReadCsvFile(sr);
                }
            } catch(Exception ex) {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if(employeeDataGridView.SelectedRows.Count != 0) {
                DataGridViewRow row = this.employeeDataGridView.SelectedRows[0];
                cmbHairColor.SelectedItem = row.Cells[5].Value.ToString();
            } else {
                cmbHairColor.SelectedIndex = 0;
            }
        }

        private void btnSave_Click(object sender, EventArgs e) {
            Employee newEmployee = new Employee();
            newEmployee.name = txtName.Text;
            newEmployee.dob = dobPicker.Text;
            newEmployee.age = Int32.Parse(txtAge.Text);
            if(radioMale.Checked) {
                newEmployee.gender = "Male";
            } else if(radioFemale.Checked) {
                newEmployee.gender = "Female";
            } else {
                newEmployee.gender = "Male";
            }

            if(chkBoxSmoking.Checked) {
                newEmployee.smoking = "Yes";
            } else {
                newEmployee.smoking = "No";
            }

            newEmployee.hairColor = cmbHairColor.SelectedItem.ToString();

            List<Employee> lst = (List<Employee>)employeeBindingSource.List;

            bool isFound = false;

            for(int i = 0; i < lst.Count; i++) {
                Employee employee = lst.ElementAt(i);
                if(employee.name.Equals(txtName.Text)) {
                    employeeBindingSource.Remove(employee);
                    employeeBindingSource.Insert(i, newEmployee);
                    isFound = true;
                    break;
                }
            }

            if(!isFound) {
                employeeBindingSource.Add(newEmployee);
            }
        }

        private void button1_Click(object sender, EventArgs e) {
            try {
                using(StreamWriter sw = new StreamWriter(path)) {
                    WriteCsvFile(sw);
                }
                MessageBox.Show("Data saved.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            } catch(Exception ex) {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void employeeDataGridView_SelectionChanged(object sender, EventArgs e) {
            if(employeeDataGridView.SelectedRows.Count != 0) {
                DataGridViewRow row = this.employeeDataGridView.SelectedRows[0];
                txtName.Text = row.Cells[0].Value.ToString();
                dobPicker.Text = row.Cells[1].Value.ToString();
                txtAge.Text = row.Cells[2].Value.ToString();

                if(row.Cells[3].Value.ToString().Equals("Male")) {
                    radioMale.Checked = true;
                } else if(row.Cells[3].Value.ToString().Equals("Female")) {
                    radioFemale.Checked = true;
                } else {
                    radioMale.Checked = true;
                }

                if(row.Cells[4].Value.ToString().Equals("Yes")) {
                    chkBoxSmoking.Checked = true;
                } else {
                    chkBoxSmoking.Checked = false;
                }

                cmbHairColor.SelectedItem = row.Cells[5].Value.ToString();
            }
        }
    }
}
