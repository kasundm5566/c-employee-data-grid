﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManagement {
    public class Employee {
        //public enum Gender {
        //    male, female
        //}
        public string name { get; set; }
        public string dob { get; set; }
        public int age { get; set; }
        public string gender { get; set; }
        public string smoking { get; set; }
        public string hairColor { get; set; }
    }
}
