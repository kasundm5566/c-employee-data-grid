# README

This README has documented an overview for the project.

### What is this repository for?

* This is a sample C# Form Application developed to implement the use of DataGridView.

### prerequisites

* Visual Studio 2015 Express

### Run the application

* Open the project in Visual Studio.
* Run the application from the menu bar.

### Important notes

* All the data appeared in the data grid are stores in a comma separated file (.csv).
* File named Employee_data.csv will be stored inside the project directory.
* Since this is a simple demonstration app assumes field **name** as the unique identification field **(Do not duplicate it)**.
* Add/Update button: If the **name** is already there, system will update it, otherwise new record will be added.
* Finalize updates button: Writes data to the csv file.